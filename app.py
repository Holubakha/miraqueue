import logging
from flask import Flask, request
from flask_sqlalchemy import SQLAlchemy
from flask_login import LoginManager
from flask_mqtt import Mqtt

app = Flask(__name__)

app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///db.db'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['SECRET_KEY'] = 'A)D(*@P}{we'

app.config['MQTT_BROKER_URL'] = 'localhost'
app.config['MQTT_TRANSPORT'] = 'websockets'
app.config['MQTT_BROKER_PORT'] = 9001
app.config['MQTT_REFRESH_TIME'] = 1.0

mqtt = Mqtt(app)

@app.after_request
def after_request(response):
    print(request.method, request.full_path, request.form)
    return response

db = SQLAlchemy(app)

logging.basicConfig()
logging.getLogger('sqlalchemy.engine').setLevel(logging.INFO)

lman = LoginManager()
lman.init_app(app)
lman.login_view = 'login'
