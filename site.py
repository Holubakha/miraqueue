from flask import Flask, render_template
app = Flask(__name__)

@app.route('/', methods=["GET"])
def index():
    return render_template("index.html")


@app.route('/feedback', methods=["GET"])
def feedback():
    return render_template("feedback.html")

@app.route('/admin/home', methods=["GET"])
def home():
    queues = [{'name':'Перша черга', 'id':'0'}, {'name':'Друга черга', 'id':'1'}, {'name':'Третя черга', 'id':'2'}]
    return render_template("home.html", queues=queues, name='Адміністрація', description='Справки та свідоцтва')

@app.route('/registration', methods=["GET"])
def registration():
    return render_template("registration.html")

@app.route('/login', methods=["GET"])
def login():
    return render_template("login.html")


if __name__  == "__main__":
    app.run(debug=True)