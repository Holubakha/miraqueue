from flask_login import current_user
from flask import Blueprint, request, session
from flask import render_template, redirect, jsonify, g
from app import app, db
from model import Consumer, Session
from werkzeug.security import check_password_hash
from functools import wraps

def auth_required(f):
    @wraps(f)
    def wrapper(*args, **kwargs):
        if not 'access_token' in request.args:
            return jsonify(error='Authorization required')
        sess = Session.query.filter_by(session_id=request.args['access_token']).first()
        if not sess:
            return jsonify(error='Invalid session')
        g.current_user = Consumer.query.get(sess.user_id)
        return f(*args, **kwargs)
    return wrapper
